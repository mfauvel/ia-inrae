#+TITLE: IA et imagerie satellitaire pour la cartographie des territoires agri-forestiers
#+SUBTITLE: Journée \emph{Intelligence Artificielle en Agriculture}
#+AUTHOR: Mathieu Fauvel
#+EMAIL: mathieu.fauvel@inrae.fr
#+DATE: 28 février 2020

#+LANGUAGE: fr
#+OPTIONS: toc:t H:2 tags:nil d:nil

#+LaTeX_CLASS_OPTIONS: [pressentation,10pt,aspectratio=1610,xcolor=table, serif]
#+BEAMER_THEME: Cesbio
#+BEAMER_HEADER: \usefonttheme[onlymath]{serif}


#+LATEX_HEADER: \newcommand{\shorttitle}{IA et imagerie satellitaire}
#+LATEX_HEADER: \newcommand{\shortauthor}{M. Fauvel - @MaFauvel}
#+LATEX_HEADER: \institute{CESBIO, Universit{\'e} de Toulouse, CNES/CNRS/INRAe/IRD/UPS, Toulouse, FRANCE}
#+LATEX_HEADER: \titlegraphic{\includegraphics[width=0.2\textwidth]{./figures/logo_cesbio_transp.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_inrae.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_ANITIvec.pdf}~\vspace{3cm}}

# #+BEAMER_HEADER: \setbeamercovered{again covered={\opaqueness<1->{25}}}


#+LATEX_HEADER: \usepackage[french]{babel}\usepackage{etex}\usepackage{pifont}\usepackage{booktabs}\usepackage{collcell}
#+LATEX_HEADER: \usepackage{tikz}\usepackage{amsmath}\usepackage[T1]{fontenc}\usepackage{lmodern}\usepackage[babel=true,kerning=true]{microtype}
#+LATEX_HEADER: \usepackage{pgfplots,pgfplotstable}\usetikzlibrary{dateplot,mindmap,trees,shapes,arrows,spy,3d,backgrounds,positioning,pgfplots.statistics,calc,fit,overlay-beamer-styles, decorations.markings}\usepgfplotslibrary{groupplots}\pgfplotsset{compat=newest}
#+LATEX_HEADER: \pgfplotsset{/pgf/number format/assume math mode=true}\usepackage{csquotes}
# \usepackage[T1]{fontenc}\usepackage{textcomp,lmodern}\usepackage[small,T1}{eulervm}
#+LATEX_HEADER: \renewenvironment{description}{\begin{itemize}}{\end{itemize}}\usepackage{smartdiagram}
#+LATEX_HEADER: \usepackage[citestyle=alphabetic,bibstyle=authortitle]{biblatex}\addbibresource{refs.bib}
#+LATEX_HEADER: \usepackage[type={CC}, modifier={by-sa}, version={4.0},]{doclicense}

#+LATEX_HEADER: \usepackage{pgfpages}
#+LATEX_HEADER: \setbeameroption{hide notes}

* Imagerie Satellitaire Pour L'observation des surfaces continentales :export:
#+LaTeX: \pgfplotsset{compat=newest}
** Images de télédétection

Une image de télédétection est un échantillonage d'un processus spatial, spectral et temporel.

#+BEGIN_EXPORT latex
\begin{center}
  \begin{tikzpicture}[spy using outlines={circle, magnification=3, size=1.75cm, connect spies}]
    \visible<1>{\node at (-4,0) {\includegraphics[width=4cm]{figures/46.jpg}};
      \node at (1.25,-2.5) {\begin{axis}[xmin=407,xmax=985,ymin=0,ymax=1,grid,width=7cm,height=4cm,footnotesize,axis x line=left,axis y line=left]
          \addplot[thick,smooth] file {figures/spectre_full.txt};
        \end{axis}};}
    \visible<2>{\node at (-4,0) {\includegraphics[width=4cm]{figures/46_8.jpg}};
      \draw[very thin] (-6,-2) grid[step = 0.125] (-2,2);        
      \node at (1.25,-2.5) {\begin{axis}[xmin=407,xmax=985,ymin=0,ymax=1,grid,width=7cm,height=4cm,footnotesize,axis x line=left,axis y line=left]
          \addplot[thick,smooth] file {figures/spectre_8.txt};
        \end{axis}}; 
    } 
    \visible<3-4>{\node at (-4,0) {\includegraphics[width=4cm]{figures/46_8.jpg}};
      \draw[very thin] (-6,-2) grid[step = 0.125] (-2,2);
      \node at (1.25,-2.5) {\begin{axis}[xmin=407,xmax=985,ymin=0,ymax=1,grid,width=7cm,height=4cm,footnotesize,axis x line=left,axis y line=left]
          \addplot[thick,mark=*,only marks] file {figures/spectre_ss.txt};
        \end{axis}};
    }
    \visible<4>{\draw[->,line width= 1pt] (-6.2,-3.5) -- (5.75,-3.5) node[above] {$t$};
      \foreach \x / \xtext in {-6/Janvier,-5/Février,-4/Mars,-3/Avril,-2/Mai,-1/Juin,0/Juillet,1/\textcolor{CESBIOgreen}{Aout},2/Septembre,3/Octobre,4/Novembre,5/Decembre}{
        \fill (\x,-3.5) circle [radius=2pt];
        \node at (\x,-4) {\rotatebox{30}{\footnotesize\xtext}};
      }
    }
    \spy [gray] on (-4.5,1.5) in node [left] at (0.5,1.5);
    \fill[gray] (-4.46,1.53) circle (0.02);
    \fill[white] (4,0) rectangle +(0.04,0.04);
  \end{tikzpicture}
\end{center}
#+END_EXPORT
*** Note                                                         :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Mesure de reflectance
- Echantillonage d'un processus -> réflectance du paysage
- Les 3 domaines
- Transitions: pourquoi cette reflectance nous intéresse t elle ?

** Signature spectrale
#+begin_export latex 
\shorthandoff{;}
\begin{center}
  \begin{tikzpicture}
    \begin{axis}[xmin=0.4,xmax=2.5,ymin=0,ymax=1,grid,xlabel=$\lambda~({\mu}m)$,ylabel=Réflectance,width=0.9\linewidth,height=0.5\linewidth,legend style={fill=none, at={(0.5,-0.2)},anchor=north}, legend columns=4]
      \addplot+[mark=none,thick,smooth, red!50] file {figures/oak.txt};
      \pgfplotstableread{figures/grass.txt}\loadedtable
      \addplot+[mark=none,smooth,thick, blue!50] table[x=wave,y=grass] from \loadedtable;
      \addplot+[mark=none,smooth,thick, orange!50] table[x=wave,y=drygrass] from \loadedtable;
      \pgfplotstableread{figures/talc.txt}\loadtable
      \addplot+[mark=none,smooth,thick,violet!50] table[x=wave,y=talc] from \loadtable;
      \legend{Chêne, Pelouse, Pelouse Sèche, Talc}
    \end{axis}
  \end{tikzpicture}
\end{center}
\shorthandon{;}
#+end_export

*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Carte d'identité de la surface couverte par le pixel
- Présenter les trois matériaux
  + Pigments photo-synthèse
- La réflectance est influencé par l'état phyto-sanitaire de la végétation -> exemple pelouse
- Transition: pourquoi aller voir plus loin que le visible ?
** Comment vont-elles ?
*** Color Image                                                 :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <1>
:END:
#+ATTR_LATEX: :width 0.9\linewidth :centering
[[file:./figures/demo.pdf]]

*** Bleu Band                                                   :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <2>
:END:
#+ATTR_LATEX: :width 0.9\linewidth :centering
[[file:./figures/demo_1.pdf]]

*** Green Band                                                  :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <3>
:END:
#+ATTR_LATEX: :width 0.9\linewidth :centering
[[file:./figures/demo_2.pdf]]

*** Red Band                                                    :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <4>
:END:
#+ATTR_LATEX: :width 0.9\linewidth :centering
[[file:./figures/demo_3.pdf]]

*** Infra Red band                                              :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <5>
:END:
#+ATTR_LATEX: :width 0.9\linewidth :centering
[[file:./figures/demo_5.pdf]]

*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Chaque domaine de longueur d'ondes apporte une information particulière/nouvelle
** Applications
:PROPERTIES:
:BEAMER_opt: fragile
:END:
#+BEGIN_EXPORT latex
\tikzset{grow cyclic list/.code={%
    \def\tikzgrowthpositions{{#1}}%
    \foreach \n [count=\i,remember=\i]in {#1}{}%
    \let\tikzgrowthpositionscount=\i%
    \tikzset{growth function=\tikzgrowcycliclist}}}
\def\tikzgrowcycliclist{%
  \pgftransformshift{%
    \pgfpointpolar{\tikzgrowthpositions[mod(\the\tikznumberofcurrentchild-1,\tikzgrowthpositionscount)]}%
    {\the\tikzleveldistance}}}

\begin{center}
\resizebox{0.85\textwidth}{!}{
  \begin{tikzpicture}[<->,mindmap,every node/.append style={concept,execute at begin node=\hskip0pt},grow cyclic,
    level 1/.append style={level distance=4.25cm,sibling angle=72,every child/.append style={concept color=black,text=white,font=\bfseries}},%
    level 2/.append style={level distance=3cm,every child/.append style={concept color=gray!75,text=black,font=\small},sibling angle=45},%sibling angle=45
    root concept/.append style={concept color=black, fill=white, line width=1ex, text=black,font=\large},
    ]
    \node [root concept] (hyper) {\textsc{Télédétection}}[grow cyclic list={45,-12.5,-140,115,-60,164}] 
    child { node  (lm) {Environnement}[clockwise from=90]
      child {node (biomass) {Biomasse}}
      child {node (biodiversity) {Biodiversité}}
      child {node (lulc) {Couverture des sols}}
      child {node (cd) {Détection changements}}
    }
    child[level distance= 6cm] {node (geology) {Géologie}[clockwise from=15]
      child {node (det) {Détection mineraies}}
      child {node (soil) {Composition sols}}
    }
    child {node (pa) {Agriculture de précision}[clockwise from=-120]
      child {node (ns) {Stress nutritif}}
      child {node (ws) {Stress hydrique}}
      child {node (pp) {Maladies}}
    }
    child {node (hydrology) {Hydrologie}[clockwise from=180]
      child {node (wq) {Qualité des eaux}}
      child {node (coast) {Zones cotières}}
    }
    child {node (military) {Militaire}[clockwise from=0]
      child {node (target) {Détection de cible}}
      child {node (carto) {Cartographie}}
    }
    child[level distance= 6cm] {node (urban) {Urbain}[clockwise from=180]
      child {node (target) {Polution}}
      child {node (target) {Expansion}}
    };
    \end{tikzpicture}}
\end{center}
#+END_EXPORT
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Ceci est représentatif mais non-exhaustif
- Je vais vous parler plutôt des applis "Environnement"
- Transition: "Depuis 4/5 ans, il y a une révolution sur la données disponible, avec les mission Sentinel issues du programme COPERNICUS de l'ESA"
** Constellation optique Sentinel-2
- Couverture complète des terres émergées
- Haute résolution spatiale (10m & 20m)
- Fréquence de revisite: 5 jours, avec angle de vue constants
- 13 bandes spectrales, visible+proche/moyen infra rouge
- Données "gratuites et ouvertes", prêts à l'emploi en France (Pôle Theia)
- http://osr-cesbio.ups-tlse.fr/echangeswww/majadata/spot_differences.html
- La même chose en radar !
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Lire les propriétés
- Faire la démo
- Transition: Autant de données avec de telles résolutions apportent son lot de compléxité pour la partie "extraction d'information automatique"
* Problèmes en analyse de données de télédétection                   :export:
** Volume
#+begin_export latex
\begin{center}
    \begin{tikzpicture}
      \begin{axis}[ybar stacked,grid,small,bar shift=0pt,width=0.65\linewidth,legend style={fill=none},
        xtick=data,bar width=15pt,ymin=0,ymax=4, symbolic x coords={2016,2017,2018,2019},
        ylabel={Volume de donnée (PB)}, xtick=data, xlabel = {Année}, title={Volume annuel estimé de donnée~\cite{Soille_2018}.},legend pos=outer north east]
        \addplot[red!50, fill=red!50] plot coordinates {(2016, 0.8) (2017, 1.7) (2018,2.1) (2019,2.1)};
        \addplot[blue!50, fill=blue!50] plot coordinates {(2016, 0.45) (2017, 1) (2018,1.5) (2019,1.5)};
        \legend{\strut Sentinel 1, \strut Sentinel 2};   
      \end{axis}   
    \end{tikzpicture}
\end{center}
#+end_export
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
Travailler de manière distribuée
+ Parallélisé les traitements
+ Donnée disponibles à des endroits différents

** Hétérogénéité des données
*** Multiresolution spatiale                                    :B_onlyenv:
:PROPERTIES:
:BEAMER_act: <1-5>
:BEAMER_env: onlyenv
:END:
#+BEGIN_EXPORT latex
\begin{center}
\begin{tikzpicture}[scale=0.8,every node/.style={minimum size=1cm},on grid]
  \begin{scope}[yshift=-2cm,every node/.append style={yslant=0.5,xslant=-1},yslant=0.5,xslant=-1]
    \draw[black,] (0,0) grid[step=1] (5,5);
    \fill[red!50] (2.05,2.05) rectangle (2.95,2.95); % center pixel
  \end{scope}
  
  \visible<2-5>{\begin{scope}[yshift=-1cm,every node/.append style={yslant=0.5,xslant=-1},yslant=0.5,xslant=-1]
      \draw[fill=gray!25,fill opacity=0.75] (0,0) rectangle (5,5);
      \draw[step=2.5mm, black,] (0,0) grid (5,5);
      \fill[red!50] (2.05,2.05) rectangle (2.95,2.95); % center pixel
    \end{scope}
  }
  % 
  \visible<3-5>{\begin{scope}[yshift=0,every node/.append style={yslant=0.5,xslant=-1},yslant=0.5,xslant=-1]
      \draw[fill=gray!25,fill opacity=0.75] (0,0) rectangle (5,5);
      \draw[black,] (0,0) grid[step=1] (5,5);
      \fill[red!50] (2.05,2.05) rectangle (2.95,2.95); % center pixel
    \end{scope}
  }

  \visible<4-5>{\begin{scope}[yshift=1cm,every node/.append style={yslant=0.5,xslant=-1},yslant=0.5,xslant=-1]
      \draw[fill=gray!25,fill opacity=0.75] (0,0) rectangle (5,5);
      \fill[blue!50] (0,2) -- (4,2) -- (4,4.5) -- (2,4.5) -- (2,3) -- (0,3) -- (0,2);
      \fill[red!50] (2.05,2.05) rectangle (2.95,2.95); % center pixel
    \end{scope}
  }

  \visible<5>{\begin{scope}[yshift=2cm,every node/.append style={yslant=0.5,xslant=-1},yslant=0.5,xslant=-1]
      \draw[fill=gray!25,fill opacity=0.75] (0,0) rectangle (5,5);
      \fill[red!50] (2.05,2.05) rectangle (2.95,2.95); % center pixel
      \fill[blue!50] (2.25,2.25)  circle (0.125cm);
      \fill[blue!50] (2.75,2.75)  circle (0.125cm);
      \fill[blue!50] (3.75,1.75)  circle (0.125cm);
      \fill[blue!50] (1.75,3.75)  circle (0.125cm);
      \fill[blue!50] (3.75,2.75)  circle (0.125cm);
      \fill[blue!50] (2.75,1.75)  circle (0.125cm);
      \fill[blue!50] (0.75,1.25)  circle (0.125cm);
      \fill[blue!50] (4.75,4.25)  circle (0.125cm);
    \end{scope}
  }   
\end{tikzpicture}
\end{center}
#+END_EXPORT

*** S1&S2                                                       :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <6>
:END:
#+begin_export latex
\begin{center}
  \begin{tikzpicture}
    \pgfplotsset{set layers}
    \begin{groupplot}[group style={group size= 1 by 2, vertical sep=0.25cm}, date coordinates in=x,date ZERO=2017-08-15, scale only axis, axis y line*=left,
      ymin=0,ymax=1,footnotesize,width=0.75\linewidth,height=0.22\linewidth,
      yticklabel style={
        /pgf/number format/fixed,
        /pgf/number format/fixed zerofill,
        /pgf/number format/precision=1,
      },
        xticklabel=\year-\month,
        xticklabel style={
          rotate=45,
          anchor=near xticklabel,
        },
        xtick={2017-09-01,2017-10-01,2017-11-01,2017-12-01,2018-01-01,2018-02-01,2018-03-01,2018-04-01,2018-05-01,2018-06-01,2018-07-01,2018-08-01,2018-09-01,2018-10-01,2018-11-01,2018-12-01},
        xmin=2017-08-25,
        xmax=2018-12-10,
        ylabel=NDVI]
      
      \nextgroupplot[xticklabels=\empty,]
      \addplot[ybar,gray,thick,fill=gray, bar width=0.5pt, fill opacity=0.5, draw opacity=0.5] table [x = Dates, y expr ={\thisrow{M3}==0? nan:\thisrow{M3}},col sep=comma] {figures/mask_clouds.csv};
      \addplot[ mark=None, very thick, red!50] table [x=Dates,y=Q1,col sep=comma] {figures/pixels_plot_943_2647.csv};
      
      \nextgroupplot[]
      \addplot[ybar,gray,thick,fill=gray, bar width=0.5pt, fill opacity=0.5, draw opacity=0.5] table [x = Dates, y expr ={\thisrow{M1}==0? nan:\thisrow{M1}},col sep=comma] {figures/mask_clouds.csv};
      \addplot[ mark=None, very thick, red!50] table [x=Dates,y=Q1,col sep=comma] {figures/pixels_plot_1156_3001.csv};

    \end{groupplot}
    \begin{groupplot}[group style={group size= 1 by 2, vertical sep=0.25cm},
      date coordinates in=x,date ZERO=2017-08-15, 
      scale only axis,
      axis x line=none, axis y line*=right,
      ymin=-14,ymax=0,footnotesize,width=0.75\linewidth,height=0.22\linewidth,
      xmin=2017-08-25,
      xmax=2018-12-10,
      ylabel=\(\gamma^O(VV)\)]      
      \nextgroupplot[]
      \addplot[ mark=None, very thick, blue] table [x=Dates,y expr={10*log10(\thisrow{Q1})},col sep=comma] {figures/pixels_S1_plot_943_2647.csv};
      
      \nextgroupplot[ymax=0.2]
      \addplot[ mark=None, very thick, blue] table [x=Dates,y expr={10*log10(\thisrow{Q1})},col sep=comma] {figures/pixels_S1_plot_1156_3001.csv};
    \end{groupplot}
  \end{tikzpicture}
\end{center}
#+end_export
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Données hétérogènes
  + Des données multi-temporelles, à THRS
  + Grilles d'échantillonnages spatiales différentes
- S1 versus S2
  - Dynamique temporelle différents
  - Bruit dans la données différents (additif, multiplicatif)
** Bruit dans les mesures
*** Tracks                                                      :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <1>
:END:
#+BEGIN_CENTER
#+ATTR_LATEX: :width 0.725\linewidth
[[file:./figures/S2_tracks.png]]
#+END_CENTER
*** Clouds                                                      :B_onlyenv:
:PROPERTIES:
:BEAMER_act: <2->
:BEAMER_env: onlyenv
:END:
#+BEGIN_EXPORT latex
\begin{center}
\only<2>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160428-104500-651_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<3>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160518-104028-461_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<4>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160528-104248-160_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<5>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160607-104026-455_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<6>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160627-104023-463_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<7>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160707-104025-456_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<8>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160717-104833-511_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<9>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160806-104026-455_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}

\only<10>{\includegraphics[width=0.52\linewidth]{figures/red_SENTINEL2A_20160816-104025-461_L2A_T31TEJ_D_V1-0_FRE_B2.jpg}}
\end{center}
#+END_EXPORT
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- *Track* Grilles temporelles différentes
- *Nuages* Echantillonage irrégulier (ce que je vous ai montré précédemment n'est pas la donnée brutes)
** Bruit dans les labels
#+ATTR_LATEX: :width 0.6\textwidth
[[file:figures/BD_ORTHO_CLC_Bati_BDTOPO_All.jpeg]]
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Pas de correspondance entre les bases de données et les SITS
- BD pas à jours
- Strategie de simplification/mise en correspondance -> *Bruit dans les labels*
* Exemples d'application                                             :export:
** Occupations des sols
:Notes:
- Représentation spectrale et spatiale
- Bruit dans les labels, BD pas à jours,
- Échantillonnage temporel
:END:
*** Classification supervisée                                     :B_block: 
:PROPERTIES: 
:BEAMER_env: block 
:END: 
- Profil spectro-temporel \(\mathbb{R}^{1000}\)
  $$\mathbf{x} = \big[\lambda_1(t_1),  \ldots, \lambda_1(t_T), \ldots, \lambda_{10}(t_T), \ldots, \text{si}_{1}(t_1), \ldots, \text{si}_{p}(t_T) \ldots\big]$$
- Stratification par zone-écoclimatique
- /Random Forests/: 
  + Rapide, robuste et stable
  + Pas d'information de dépendance spatiale 
*** Donnée de référence                                           :B_block: 
:PROPERTIES: 
:BEAMER_env: block   
:BEAMER_act: <2-> 
:END: 
Fusion de bases de donnée "anciennes" et hétérogènes
- Corine Land Cover
- RPG pour les classes agricoles
- BD TOPO pour la forêt
- Urban Atlas pour les classes artificielles
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Travail à l'échelle du pixel
  + reconstruction du profil (interpolation, MSE critère qui ne s'occupe pas de l'objectif final)
  + Indépendance spatiale des pixels
- Mise à jours annuelle France metropolitaine + corse
- 20 To de données à traiter 
** OSO 2018 cite:Inglada_2017
*** Carte France entière                                        :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <1>
:END:
#+BEGIN_CENTER
#+ATTR_LATEX: :width 0.65\linewidth
[[file:./figures/ocs_2018.png]]
#+END_CENTER
*** Zoom                                                        :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <2-5>
:END:
#+begin_export latex
\begin{center}
  \includegraphics<2>[width=0.72\textwidth]{figures/crop_ortho_1.png}%
  \includegraphics<3>[width=0.72\textwidth]{figures/crop_oso_1_color.png}%
  \includegraphics<4>[width=0.72\textwidth]{figures/crop_ortho_2.png}%
  \includegraphics<5>[width=0.72\textwidth]{figures/crop_oso_2_color.png}%
\end{center}
#+end_export
*** Villani                                                     :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <6>
:END:
#+BEGIN_CENTER
#+ATTR_LATEX: :width 0.85\linewidth
[[file:./figures/villani_sia.jpg]]
#+END_CENTER
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Transitions pour villani: ce sont des résultats que nous avons discuté au SIA, et ...
- Il aurait dit "j'adore ce que vous faites"
** Diversité Taxonomique cite:Fauvel_2020
:PROPERTIES:
:BEAMER_opt: fragile
:END:
*** Fleur                                                       :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <1>
:END:
#+BEGIN_CENTER
[[https://www.psdr-occitanie.fr/PSDR4-Occitanie/Le-projet-SEBIOREF-Services-Ecosystemiques-rendus-par-la-biodiversite][Projet Sebioref]]

#+ATTR_LATEX: :width 0.5\linewidth
[[file:./figures/fleur_prairies.png]]
#+END_CENTER
*** Precision                                                   :B_onlyenv:
:PROPERTIES:
:BEAMER_act: <2>
:BEAMER_env: onlyenv
:END:
#+begin_export latex
\begin{center}
\begin{tabular}{lllrr}
\toprule
\textbf{Données} & \textbf{Variable} & \textbf{Méthode} & \(\hat{r}^2\) & \(\hat{\sigma}_{r^2}\)\\
\midrule
R IR & Indice de Simpson & RF & 0.45 & 0.13\\
R IR & Indice de Shannon & RF & 0.43 & 0.13\\
S2 & Diversité en couleur & RF & 0.40 & 0.08\\
S1 S2 & Richesse & KRidge & 0.34 & 0.15\\
S1 S2 & Dépendance aux insectes & KRidge & 0.32 & 0.15\\
S1 S2 & Richesse en fleurs & KRidge & 0.32 & 0.16\\
S1 S2 & Indice de récompense & GP RBF & 0.32 & 0.20\\
\bottomrule
\end{tabular}
\end{center}
#+end_export
*** Sub image                                                   :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <3-10>
:END:
#+begin_export latex
\newcommand{\colorbar}[3][viridis]{
     \begin{tikzpicture}
    \pgfmathsetmacro{\nsamp}{5};
    \pgfmathsetmacro{\step}{#2+(#3-#2)/(\nsamp-1)};
         \begin{axis}[footnotesize,
	         xmin=0,xmax=1,
	         ymin=0,ymax=1,
                 hide axis,
                 scale only axis,
                 height=0pt,
                 width=0pt,
                 colormap/#1,
                 colorbar sampled,
                 colorbar,
                 point meta min=#2,
                 point meta max=#3,
                 colorbar style={
                   font=\tiny,
                   scaled y ticks = true,
                   tick label style={/pgf/number format/fixed},
                   %samples=\nsamp,
                   height={0.25\linewidth},
                   width={0.15cm},
                   ytick={#2,\step,...,#3},
                   /pgf/number format/precision=2,
                   /pgf/number format/fixed,
           }]
         \end{axis}
     \end{tikzpicture}
}
\only<3>{
 \begin{center}
            \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_4_308.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_4_308_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_4_308_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<4>{
 \begin{center}
        \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_103_5502.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_103_5502_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_103_5502_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<5>{
 \begin{center}
        \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_133_205.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_133_205_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_133_205_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<6>{
 \begin{center}
         \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_186_914.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_186_914_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_186_914_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<7>{
 \begin{center}
        \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_274_3448.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_274_3448_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_274_3448_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<8>{
 \begin{center}
         \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_282_3653.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_282_3653_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_282_3653_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<9>{
 \begin{center}
        \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_512_1010.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_512_1010_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_512_1010_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}

\only<10>{
 \begin{center}
        \begin{tabular}{ccccc}
            \includegraphics[width=0.25\linewidth]{figures/CropImages/ortho_667_827.jpg} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_667_827_mean.png} &
            \colorbar{0.00347164982667}{0.914736632863} &
            \includegraphics[width=0.25\linewidth]{figures/CropImages/Simpson_667_827_std.png}
            \colorbar{0.00840147008824}{0.400643715975} &
            \end{tabular}
 \end{center}
}
#+end_export
*** Density                                                     :B_onlyenv:
:PROPERTIES:
:BEAMER_env: onlyenv
:BEAMER_act: <11>
:END:
#+begin_export latex
\begin{center}
  \begin{tikzpicture}
    \begin{groupplot}[group style={group size=3 by 2, vertical sep=1.25cm},
        xmin=0, xmax=1, ymin=0, ymax=9.0,
        width=0.33\linewidth,height=0.275\linewidth,
        xlabel style={font=\scriptsize},
        title style={font=\scriptsize},
        yticklabel style={font=\scriptsize,
          /pgf/number format/fixed,
          /pgf/number format/fixed zerofill,
          /pgf/number format/precision=1,
        },
        xticklabel style={font=\scriptsize,
          /pgf/number format/fixed,
          /pgf/number format/fixed zerofill,
          /pgf/number format/precision=1,
        },
        grid]
      \nextgroupplot[title={Jachère (\(\leq 5\) années)}]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=J5M, col sep=comma] {figures/dfPdf.csv};
      \nextgroupplot[title={Jachère (\(\geq 6\) années)}]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=J6S, col sep=comma] {figures/dfPdf.csv};
      \nextgroupplot[title={Prairie permanente}]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=PPH, col sep=comma] {figures/dfPdf.csv};
      \nextgroupplot[title={Prairie en rotation longue (\(\geq 6\) années)}]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=PRL, col sep=comma] {figures/dfPdf.csv};
      \nextgroupplot[title={Prairie temporaire}]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=PTR, col sep=comma] {figures/dfPdf.csv};
      \nextgroupplot[title={Surface pastorale (ressources herbeuses) }]
      \addplot[CESBIOgreen, fill=CESBIOgreen!50] table [x=x, y=SPH, col sep=comma] {figures/dfPdf.csv};
    \end{groupplot}
    \node (title) at ($(group c1r1.center)!0.5!(group c3r1.center)+(0,2.25cm)$) {Distribution de l'indice de Simpson};
  \end{tikzpicture}
\end{center}
#+end_export
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Variables la biodiversité
- Approches similaires à la précédente pour la formulation du problèmes
- Mais résultats moins bon, moins spatialisable
- Première prédiction à l'échelle intra-parcellaire
- (en fonction du temps) -> Analyse à l'échelle "paysagère"
** Services Ecosystémiques
:PROPERTIES:
:BEAMER_opt: fragile
:END:
*** Modélisation paysagère continue
#+BEGIN_EXPORT latex
\begin{center}
  \begin{tabular}{cc}
    \includegraphics[width=.25\textwidth]{figures/ndvi_13.png}
    &
      \only<1>{\begin{tikzpicture}  \begin{axis}[title={$p_{i}$},ymin=0,ymax=4.5,xmin=0,xmax=1,grid,width=0.7\textwidth,height=0.3\textwidth,xtick={0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1}]
          \addplot[ybar, hist={bins=75, data min=0, data max=1,density},fill=blue!50] table [y index=0] {figures/ndvi.csv};
          \draw[thick,orange] (0.621968,0) -- (0.621968,5);
          \draw[thick,orange,dashed] (0.942963,0) -- (0.942963,5);
          \draw[thick,orange,dashed] (0.0129488,0) -- (0.0129488,5);
          \draw[thick,orange,<->] (0.376617,4) -- (0.867319,4);
        \end{axis}
      \end{tikzpicture}}\only<2->{\begin{tikzpicture}    
    \begin{axis}[title={$p_{i}$},ymin=0,ymax=4.5,xmin=0,xmax=1,grid,width=0.7\textwidth,height=0.3\textwidth,xtick={0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1}]
        \addplot [ybar,hist={bins=75, data min=0, data max=1,density} ] table [y index=0] {figures/ndvi.csv};
        \addplot [thick,mark=None,purple] table[col sep=comma, x index = 0, y index = 1] {figures/gmm_density.csv};
        \addplot [thick,mark=None,orange] table[col sep=comma, x index = 0, y index = 2] {figures/gmm_density.csv};
        \addplot [thick,mark=None,green] table[col sep=comma, x index = 0, y index = 3] {figures/gmm_density.csv};
        \addplot [thick,mark=None,magenta] table[col sep=comma, x index = 0, y index = 4] {figures/gmm_density.csv};
        \addplot [thick,mark=None,cyan] table[col sep=comma, x index = 0, y index = 5] {figures/gmm_density.csv};
        \addplot [thick,mark=None,blue] table[col sep=comma, x index = 0, y index = 6] {figures/gmm_density.csv};
        \addplot [ultra thick,mark=None,black] table[col sep=comma, x index = 0, y index = 7] {figures/gmm_density.csv};
      \end{axis}
    \end{tikzpicture}}
  \end{tabular}
\end{center}
#+END_EXPORT
*** Distance entre paysage                                      :B_onlyenv:
:PROPERTIES:
:BEAMER_env: visibleenv
:BEAMER_act: <3>
:END:
 #+BEGIN_EXPORT latex
  \begin{eqnarray*}
    \langle\ell,\ell'\rangle&=&\int_{\mathbb{R}^d}p(\mathbf{x}|\ell)p(\mathbf{x}|\ell')d\mathbf{x}\\
 &=& \sum_{c,c'=1}^{C,C} p(c|\ell)p(c'|\ell')\frac{\exp\big[-0.5(\boldsymbol{\mu}_{c}-\boldsymbol{\mu}_{c'})^\top\big(\boldsymbol{\Sigma}_{c}+\boldsymbol{\Sigma}_{c'}\big)^{-1}(\boldsymbol{\mu}_{c}-\boldsymbol{\mu}_{c'})\big]}{|2\pi\big(\boldsymbol{\Sigma}_{c}+\boldsymbol{\Sigma}_{c'}\big)|^{0.5}}
    \end{eqnarray*}
  #+END_EXPORT
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Work in progress
- Modélisation paysagère
- Un échantillon = une distribution de probabilité
- Définir des outils pour faire de la prédiction avec cet objet
  + En HD
  + En gros volume de donnée
- Résultats du même niveau que les approches conventionnelles
* Conclusions                                                        :export:
** Enjeux
#+BEGIN_EXPORT latex
\tikzset{
  every shadow/.style={
    fill=none,
    shadow xshift=0pt,
    shadow yshift=0pt}
}
\tikzset{module/.append style={top color=\col,bottom color=\col}}
\begin{center}
  \smartdiagramset{
    font=\large,
    module minimum width=9cm,
    module minimum height=1cm,
    text width=8.5cm,
    back arrow distance=0.75,
    set color list={orange!75, yellow, blue!50, green!50!white, cyan},border color=none,
    uniform arrow color=true,arrow color=gray!50,
    }
  \smartdiagram[flow diagram]{Télédétection $\heartsuit$ Base de Données,Perfectionner/Automatiser traitements,Mutualiser outils \& références,Intégration utilisateurs}
\end{center}
#+END_EXPORT
*** Notes                                                        :B_noteNH:
:PROPERTIES:
:BEAMER_env: noteNH
:END:
- Ne pas opposer TLD et BD -> Comment établir une synergie/coopération ?
- D'un point de vue IA: (tarte à la crème: augmenter la précision)
  + Diminuer la dépendance au données terrain (volume, fraicheur)
  + Modèle interprétable
- Ressources algorithmiques & informatiques grandissantes, besoin de mutualisation
- Conception & Validation
** Bibliography
:PROPERTIES:
:BEAMER_OPT: fragile,allowframebreaks,label=
:END:      
\printbibliography
** 
:PROPERTIES:
:BEAMER_opt: noframenumbering,
:END:
#+BEGIN_CENTER
\small
\doclicenseLongText

\doclicenseImage

#+attr_latex: :width 0.25\linewidth
[[file:./figures/qrcode.png]]

https://frama.link/Z3oke5PX
#+END_CENTER
** 
:PROPERTIES:
:BEAMER_opt: label=conclusion,standout,noframenumbering
:END:
#+BEGIN_CENTER
Des questions/remarques/commentaires ?
#+END_CENTER
